#include <X11/Xlib.h>
#include <X11/Xutil.h> /* for XSizeHints */
#include <stdio.h>
#include <stdlib.h>

#define MIN 200
#define MAX 800

int main(void)
{
	int x, y, width, height, border;
	Display *disp;
	int screen;
	Window wind;
	XSizeHints *wind_size_hints;

	/* try experimenting with different top-left coordinates */
	x = y = 0;

	/* try experimenting with different widths and heights */
	width = height = 500;

	/* try experimenting with different border widths */
	border = 0;

	if (!(disp = XOpenDisplay(NULL)))
	{
		fprintf(stderr, "Could not open an X display.\n");
		return EXIT_FAILURE;
	}

	/* get the default screen of the now-opened display */
	screen = DefaultScreen(disp);

	wind = XCreateSimpleWindow(disp, RootWindow(disp, screen),
			x, y, /* the upper left start coordinates */
			width, height, /* the dimensions of the window */
			0, /* the size of window borders - 0 for none */
			BlackPixel(disp, screen), WhitePixel(disp, screen));

	if (!(wind_size_hints = XAllocSizeHints()))
	{
		fprintf(stderr, "Could not allocate memory for size hints.\n");
		return EXIT_FAILURE;
	}

	/*	define which constraints we want to use.
		in this case, we're using all three: initial, minimum, and maximum sizes
	*/
	wind_size_hints->flags = PSize | PMinSize | PMaxSize;

	/* set the minimum allowed dimensions to MIN x MIN */
	wind_size_hints->min_width = MIN;
	wind_size_hints->min_height = MIN;

	/* set the initial dimensions to width x height */
	wind_size_hints->width = width;
	wind_size_hints->height = height;

	/* set the maximum allowed dimensions to MAX x MAX */
	wind_size_hints->max_width = MAX;
	wind_size_hints->max_height = MAX;

	/* apply the size hints to the window */
	XSetWMNormalHints(disp, wind, wind_size_hints);

	/* we can free our size hints after applying them */
	XFree(wind_size_hints);

	/* map our new window to the display and flush it */
	XMapWindow(disp, wind);
	XFlush(disp);

	/* sleep for a few seconds before cleaning up and exiting */
	sleep(15);

	/* destroy the window and close the display to avoid memory leaks */
	XDestroyWindow(disp, wind);
	XCloseDisplay(disp);

	return EXIT_SUCCESS;
}

